﻿using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;

namespace Vueling.IFR
{
    /// <summary>
    /// Clase que genera la inyección de dependencias
    /// </summary>
    public class IoC : IDisposable
    {
        protected IUnityContainer container;

        #region Singleton
        private static readonly IoC current = new IoC();

        public static IoC Current
        {
            get
            {
                return current;
            }
        }
        #endregion

        #region metodos publicos
        public T Resolve<T>()
        {
            return container.Resolve<T>();
        }

        public T Resolve<T>(string name)
        {
            return container.Resolve<T>(name);
        }

        public object Resolve(Type t)
        {
            return container.Resolve(t);
        }

        public object Resolve(Type t, string name)
        {
            return container.Resolve(t, name);
        }

        public object ResolveAll<T>()
        {
            return container.ResolveAll<T>();
        }

        public IEnumerable<object> ResolveAll(Type type)
        {
            return container.ResolveAll(type);
        }
        #endregion



        #region Constructores
        static IoC()
        {
        }

        protected IoC()
        {
            container = new UnityContainer();
            container.RegisterType(
                GetType("Vueling.CORE.Contracts.IApplicationDbContext, Vueling.CORE"),
                GetType("Vueling.DAL.ApplicationDbContext, Vueling.DAL"),
                new ContainerControlledLifetimeManager());
            container.RegisterType(
                GetType("Vueling.CORE.Contracts.ITransactionManager, Vueling.CORE"),
                GetType("Vueling.Application.TransactionManager, Vueling.Application"),
                new TransientLifetimeManager());
            container.RegisterType(
                GetType("Vueling.CORE.Contracts.IRateManager, Vueling.CORE"),
                GetType("Vueling.Application.RateManager, Vueling.Application"),
                new TransientLifetimeManager());
        }

        private Type GetType(string typeString)
        {
            Type type = Type.GetType(typeString);
            if (type == null)
            {
                throw new Exception("El tipo no se ha podido resolver: " + typeString);
            }
            return type;
        }

        public void Dispose()
        {
            container.Dispose();
        }
        #endregion
    }
}