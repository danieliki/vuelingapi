﻿using System;

namespace Vueling.WS
{
    public class Helpers
    {
        public static decimal DecimalRound(decimal amount)
        {
            return decimal.Round(amount, 2, MidpointRounding.ToEven);
        }
    }
}