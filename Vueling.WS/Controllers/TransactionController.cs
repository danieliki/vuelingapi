﻿using System.Collections.Generic;
using System.Net;
using System.Web.Http;
using Vueling.CORE.Contracts;
using Vueling.CORE.Domain.Classes;
using Vueling.WS;

namespace Vueling.Controllers
{
    public class TransactionController : ApiController
    {
        private ITransactionManager _transactionManager;
        private IRateManager _rateManager;
        private const string _EUR = "EUR";

        public TransactionController(ITransactionManager transactionManager, IRateManager rateManager)
        {
            _transactionManager = transactionManager;
            _rateManager = rateManager;
        }

        public void AddTransactionFromJson()
        {
            using (WebClient wc = new WebClient())
            {
                var json = wc.DownloadString("http://quiet-stone-2094.herokuapp.com/transactions.json");
                List<Transaction> transactionList = (List<Transaction>)Newtonsoft.Json.JsonConvert.DeserializeObject(json, typeof(List<Transaction>));

                foreach (var t in transactionList)
                {
                    _transactionManager.Add(t);
                    _transactionManager.db.SaveChanges();
                }
            }
        }

        /// <summary>
        /// Método que devuelve todas las transacciones
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Transaction> GetAllTransactions()
        {
            return _transactionManager.GetAll();
        }

        /// <summary>
        /// Método que devuelve una transaccion por su sku
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public List<Transaction> GetAllTranasctionsBySku(string sku)
        {
            decimal sumAmount = 0;
          
            //Se obtienen todas las transacciones
            var transactions = _transactionManager.GetAllBySku(sku);

            //Se convierte el importe de cada una de las transacciones en Euros
            foreach (var transaction in transactions)
            {
                if (transaction.Currency != _EUR)
                {
                    var rate = _rateManager.GetRateByFromAndTo(transaction.Currency, _EUR);
                    var amount = _transactionManager.GetTransactionInEuros(transaction.Amount, rate.Conversion);

                    transaction.Amount = Helpers.DecimalRound(amount);
                    transaction.Currency = _EUR;                  
                }

                //Se suma el importe de las transacciones
                sumAmount += transaction.Amount;
            }

            //Se añade un registro json con el importe total de las transacciones
            var trans = new Transaction()
            {
                Sku = "Total Amount",
                Amount = sumAmount,
                Currency = _EUR
            };

            transactions.Add(trans);

            return transactions;
        }
    }
}