﻿using System.Collections.Generic;
using System.Web.Http;
using Vueling.CORE.Contracts;
using Vueling.CORE.Domain.Classes;

namespace Vueling.Controllers
{
    public class RateController : ApiController
    {
        private IRateManager _rateManager;
       
        public RateController(IRateManager rateManager)
        {
            _rateManager = rateManager;
        }

        /// <summary>
        /// Método que devuelve todas las conversiones
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Rate> GetAllRates()
        {
            return _rateManager.GetAll();
        }
    }
}