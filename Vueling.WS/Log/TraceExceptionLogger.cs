﻿using log4net;
using Newtonsoft.Json;
using System;
using System.Web.Http.ExceptionHandling;

namespace Vueling.WS
{
    public class TraceExceptionLogger : ExceptionLogger
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(TraceExceptionLogger));

        public override void Log(ExceptionLoggerContext context)
        {
            string postData = string.Empty;

            try
            {
                var data = ((System.Web.Http.ApiController)context.ExceptionContext.ControllerContext.Controller)
                                .ActionContext.ActionArguments.Values;

                postData = JsonConvert.SerializeObject(data);
            }
            catch(Exception ex)
            {
                Logger.FatalFormat("Fatal error WEBAPI getting data value: {0}", ex.Message);
            }

            Logger.FatalFormat("Fatal error WEBAPI: URL:{ 0} »» METHOD: { 1} »» POST DATA:{ 2} »» Exception: { 3}",
              context.Request.RequestUri, context.Request.Method, postData, context.ExceptionContext.Exception);
        }
    }
}