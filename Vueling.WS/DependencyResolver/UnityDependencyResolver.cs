﻿using System;
using System.Collections.Generic;
using System.Web.Http.Dependencies;
using Vueling.IFR;

namespace Vueling.WS
{
    public class UnityDependencyResolver : IDependencyResolver
    {

        public IDependencyScope BeginScope()
        {
            return this;
        }

        public void Dispose()
        {
            IoC.Current.Dispose();
        }

        public object GetService(Type serviceType)
        {
            try
            {
                object instance = IoC.Current.Resolve(serviceType);
                if (serviceType.IsAbstract || serviceType.IsInterface)
                {
                    return null;
                }
                return instance;
            }
            catch (Exception)
            {

                return null;
            }
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return IoC.Current.ResolveAll(serviceType);
        }
    }
}