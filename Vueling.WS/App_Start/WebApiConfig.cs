﻿using System.Net.Http.Headers;
using System.Web.Http;
using System.Web.Http.ExceptionHandling;

namespace Vueling.WS
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Log
            #region Log
            log4net.Config.XmlConfigurator.Configure();

            config.Services.Add(typeof(IExceptionLogger), new TraceExceptionLogger());
            #endregion

            // Web API routes
            #region routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute("SkuApi", "api/{controller}/{sku}", new { sku = RouteParameter.Optional });

            config.Routes.MapHttpRoute("DefaultApi", "api/{controller}/{id}", new { id = RouteParameter.Optional });
            #endregion

            //Formatea la respuesta a Json
            config.Formatters.JsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/html"));

            //Para usar inyección de dependencias
            config.DependencyResolver = new UnityDependencyResolver();
        }
    }
}
