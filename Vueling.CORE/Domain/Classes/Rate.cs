﻿using System.ComponentModel.DataAnnotations;

namespace Vueling.CORE.Domain.Classes
{
    /// <summary>
    /// Clase que representa una conversión de una moneda a otra 
    /// </summary>
    public class Rate
    {
        /// <summary>
        /// Id de la conversión
        /// </summary>
        [Key]
        public int Id { get; set; }
        /// <summary>
        /// Divisa desde la ue se hace la conversión
        /// </summary>
        public string From { get; set; }

        /// <summary>
        /// Divisa a la que se hacela conversión
        /// </summary>
        public string To { get; set; }

        /// <summary>
        /// Importe de la conversión
        /// </summary>
        public decimal Conversion { get; set; }
    }
}
