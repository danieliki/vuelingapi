﻿using System.ComponentModel.DataAnnotations;

namespace Vueling.CORE.Domain.Classes
{
    /// <summary>
    /// Clase que representa una transacción
    /// </summary>
    public class Transaction
    {
        /// <summary>
        /// Identificador de la transacción
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// Identificador del producto asociado a la transacción
        /// </summary>
        public string Sku { get; set; }

        /// <summary>
        /// Cantidad de la transacción
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// Divisa de la transacción
        /// </summary>
        public string Currency { get; set; }
    }
}
