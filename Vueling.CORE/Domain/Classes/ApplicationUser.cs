﻿using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Vueling.CORE.Domain.Classes
{
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }

        /// <summary>
        /// Nombre propio del usuario
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Primer apellido del usuario
        /// </summary>
        public string SurName1 { get; set; }

        /// <summary>
        /// Segundo apellido del usuario
        /// </summary>
        public string SurName2 { get; set; }

        /// <summary>
        /// Dirección donde vive el usuario
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// Código postal donde vive el usuario
        /// </summary>
        public string ZipCode { get; set; }

        /// <summary>
        /// Ciudad donde vive el usuario
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// Contraseña visible del usuario
        /// </summary>
        public string Password { get; set; }
    }
}