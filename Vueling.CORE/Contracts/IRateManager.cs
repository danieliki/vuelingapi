﻿using Vueling.CORE.Domain.Classes;

namespace Vueling.CORE.Contracts
{
    public interface IRateManager : IManager<Rate>
    {
        Rate GetRateByFromAndTo(string from, string to);
    }
}
