﻿using System.Linq;

namespace Vueling.CORE.Contracts
{
    /// <summary>
    /// Interfaz de los métodos genéricos
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IManager<T>
        where T : class
    {
        IApplicationDbContext db { get; }

        IQueryable<T> GetAll(string include = null);

        T Add(T entity);

        T Delete(T entity);
    }
}
