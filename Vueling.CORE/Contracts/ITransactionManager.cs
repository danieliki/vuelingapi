﻿using System.Collections.Generic;
using Vueling.CORE.Domain.Classes;

namespace Vueling.CORE.Contracts
{
    public interface ITransactionManager : IManager<Transaction>
    {
        List<Transaction> GetAllBySku(string sku);

        decimal GetTransactionInEuros(decimal amount, decimal conversion);
    }      
}
