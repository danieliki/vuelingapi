﻿using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;
using Vueling.CORE.Contracts;
using Vueling.CORE.Domain.Classes;

namespace Vueling.DAL
{
    /// <summary>
    /// Clase de contexto de datos de la aplicacion
    /// </summary>
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>, IApplicationDbContext
    {
        public ApplicationDbContext()
            : base("DefaultConnection", false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Rate>().Property(x => x.Conversion).HasPrecision(18, 3);
            modelBuilder.Entity<IdentityRole>().HasKey(r => r.Id).Property(p => p.Name).IsRequired();
            modelBuilder.Entity<IdentityUserRole>().HasKey(r => new { r.RoleId, r.UserId });
            modelBuilder.Entity<IdentityUserLogin>().HasKey(u => new { u.UserId, u.LoginProvider, u.ProviderKey });
            base.OnModelCreating(modelBuilder);
        }

        /// <summary>
        /// Colección de datos persistibles de instrumentos
        /// </summary>
        public DbSet<Transaction> Transactions { get; set; }

        /// <summary>
        /// Colección de datos persistibles de favoritos
        /// </summary>
        public DbSet<Rate> Rates { get; set; }
    }
}
