namespace Vueling.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class IdTransaction : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.Transactions");
            AddColumn("dbo.Transactions", "Id", c => c.Int(nullable: false, identity: true));
            AlterColumn("dbo.Transactions", "Sku", c => c.String());
            AddPrimaryKey("dbo.Transactions", "Id");
        }
        
        public override void Down()
        {
            DropPrimaryKey("dbo.Transactions");
            AlterColumn("dbo.Transactions", "Sku", c => c.String(nullable: false, maxLength: 128));
            DropColumn("dbo.Transactions", "Id");
            AddPrimaryKey("dbo.Transactions", "Sku");
        }
    }
}
