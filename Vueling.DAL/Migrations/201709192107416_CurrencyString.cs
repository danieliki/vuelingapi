namespace Vueling.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CurrencyString : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Transactions", "Currency", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Transactions", "Currency", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
    }
}
