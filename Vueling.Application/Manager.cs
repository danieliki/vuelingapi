﻿using System.Linq;
using Vueling.CORE.Contracts;

namespace Vueling.Application
{
    /// <summary>
    /// Manager genérico
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class Manager<T> : IManager<T>
        where T : class
    {
        public IApplicationDbContext db { get; }

        /// <summary>
        /// Constructor del manager generico
        /// </summary>
        /// <param name="db"></param>
        public Manager(IApplicationDbContext _db)
        {
            db = _db;
        }

        /// <summary>
        /// Método que devuelve todos los resultados de una clase
        /// </summary>
        /// <returns></returns>
        public IQueryable<T> GetAll(string include = null)
        {
            if (include == null)
            {
                return db.Set<T>();
            }
            return db.Set<T>().Include(include);
        }

        /// <summary>
        /// Método que añade un registro a una clase
        /// </summary>
        /// <param name="entity"></param>
        public T Add(T entity)
        {
            return db.Set<T>().Add(entity);
        }

        /// <summary>
        /// Método que elimina un registro
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public T Delete(T entity)
        {
            return db.Set<T>().Remove(entity);
        }
    }
}
