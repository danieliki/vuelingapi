﻿using System.Linq;
using Vueling.CORE.Contracts;
using Vueling.CORE.Domain.Classes;

namespace Vueling.Application
{
    public class RateManager : Manager<Rate>, IRateManager
    {
        /// <summary>
        /// Constructor del manager
        /// </summary>
        /// <param name="db"></param>
        public RateManager(IApplicationDbContext _db)
            : base(_db)
        {
        }

        /// <summary>
        /// Método que devuelve una tarifa
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <returns></returns>
        public Rate GetRateByFromAndTo(string from, string to)
        {
            return db.Rates.FirstOrDefault(r => r.From == from && r.To == to);
        }
    }
}
