﻿using System.Collections.Generic;
using System.Linq;
using Vueling.CORE.Contracts;
using Vueling.CORE.Domain.Classes;

namespace Vueling.Application
{
    public class TransactionManager : Manager<Transaction>, ITransactionManager
    {
        /// <summary>
        /// Constructor del manager
        /// </summary>
        /// <param name="db"></param>
        public TransactionManager(IApplicationDbContext _db)
            : base(_db)
        {
        }

        /// <summary>
        /// Método que devuelve todas las transacciones por un sku
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public List<Transaction> GetAllBySku(string sku)
        {
            return db.Transactions.Where(t => t.Sku == sku).ToList();
        }

        /// <summary>
        /// Método que devuelve el importe de una transacción convertido EUR
        /// </summary>
        /// <param name="transaction"></param>
        /// <returns></returns>
        public decimal GetTransactionInEuros(decimal amount, decimal conversion)
        {
            return amount * conversion;
        }
    }
}
